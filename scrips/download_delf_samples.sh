#!/usr/bin/env bash

# From tensorflow/models/research/delf/delf/python/examples/
cd storage
mkdir delf_samples && cd delf_samples
wget http://www.robots.ox.ac.uk/~vgg/data/oxbuildings/oxbuild_images.tgz
mkdir oxford5k_images oxford5k_features
tar -xvzf oxbuild_images.tgz -C oxford5k_images/
rm oxbuild_images.tgz
cd ../..
echo storage/delf_samples/oxford5k_images/hertford_000056.jpg >> list_images.txt
echo storage/delf_samples/oxford5k_images/oxford_000317.jpg >> list_images.txt