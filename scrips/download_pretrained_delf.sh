#!/usr/bin/env bash

# From tensorflow/models/research/delf/delf/python/examples/
cd storage
mkdir parameters && cd parameters
wget http://storage.googleapis.com/delf/delf_gld_20190411.tar.gz
tar -xvzf delf_gld_20190411.tar.gz
rm delf_gld_20190411.tar.gz
cd ../..