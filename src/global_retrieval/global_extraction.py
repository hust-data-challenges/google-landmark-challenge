from src.nets.model_generating import create_model
import torch.nn.functional as F
class global_extractor(object):

    def __init__(self, model_class, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.model_class = model_class
        self.model = create_model(model_class)

    def call(self, images):
        feature = self.model(images)
        feature = F.normalize(feature, p=2, dim=1)