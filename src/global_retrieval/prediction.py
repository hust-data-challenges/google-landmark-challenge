import torch
from torch.utils.data import DataLoader
from torchvision import transforms
import os

BATCH_SIZE = 64
TTA = False
use_gpu = torch.cuda.is_available()
device = torch.device('cuda' if use_gpu else 'cpu')

if __name__ == "__main__":
    model_list = ['se_resnext50_32x4d', 'inception_v4', 'xception', 'densenet161']
    image_size_list = [320, 320, 320, 288]
    pretrained_dir = './storage/models'
    for ind, network in enumerate(model_list):
        pass