from delf.examples import extract_features
import matplotlib.image as mpimg
import cv2

from PIL import Image
from io import BytesIO

import os
import random
import string

def resize_image(image, target_size=800):
  def calc_by_ratio(a, b):
    return int(a * target_size / float(b))

  size = image.size
  if size[0] < size[1]:
    w = calc_by_ratio(size[0], size[1])
    h = target_size
  else:
    w = target_size
    h = calc_by_ratio(size[1], size[0])

  # image = image.resize((w, h), Image.BILINEAR)
  image = image.resize((w, h), Image.ANTIALIAS)
  return image

TEMP_DIR = './temp_image/'

helper = extract_features.InferenceHelper('./delf/examples/delf_config_example.pbtxt')


def get_and_cache_image(image_path, basewidth=None):
    image = Image.open(image_path)
    if basewidth is not None:
        image = resize_image(image, basewidth)
    imgByteArr = BytesIO()
    image.save(imgByteArr, format='PNG')
    imgByteArr = imgByteArr.getvalue()

    fname = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(6))
    image_path = TEMP_DIR + '{}.png'.format(fname)
    if os.path.exists(image_path):
        os.remove(image_path)
    image.save(image_path, format='PNG')

    return image_path, imgByteArr


def get_result(image_paths, size=None):
    resize_image_paths = []
    resize_image_bytes = []
    for i in range(2):
        path, byte = get_and_cache_image(image_paths[i], size)
        resize_image_paths.append(path)
        resize_image_bytes.append(byte)

    location_np_list, descriptor_np_list, feature_scale_np_list, attention_score_np_list, attention_np_list = \
        helper.get_feature_from_bytes(resize_image_bytes)

    attention_image_byte_img1 = match_images.get_attention_image_byte(resize_image_bytes[0], location_np_list[0],
                                                                      attention_np_list[0])
    attention_image_byte_img2 = match_images.get_attention_image_byte(resize_image_bytes[1], location_np_list[1],
                                                                      attention_np_list[1])

    side_by_side_comp_img_byte, score = match_images.get_ransac_image_byte(resize_image_bytes[0], location_np_list[0],
                                                                           descriptor_np_list[0], resize_image_bytes[1],
                                                                           location_np_list[1], descriptor_np_list[1])
    print('matching inliner num:', score)
    return side_by_side_comp_img_byte, attention_image_byte_img1, attention_image_byte_img2


